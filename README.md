
Perform hardening of ssh and sshd based on the Debian defaults.  This trusts the
default Debian configuration, and just adds some extra restrictions for things
we will never need.
