#!/bin/bash

set -ex

dpkg -l vagrant > /dev/null
export LC_ALL=C.UTF-8

cd "$(dirname "$0")/role-test-vm"

vagrant destroy -f
vagrant up
vagrant ssh -c 'cd /vagrant; ansible-playbook \
	--connection=local --inventory localhost, --limit localhost \
        --verbose \
        tests/playbook.yml'
ssh_config=$(mktemp)
vagrant ssh-config > "$ssh_config"

echo force obsolete algorithms, connections should fail
# the -o option should break connecting via ssh
ssh -F "$ssh_config" -oCiphers=aes256-cbc default "sudo sshd -T | grep -i ^Ciphers" && exit 1
ssh -F "$ssh_config" -oHostKeyAlgorithms=ssh-rsa default "sudo sshd -T | grep -i ^HostKeyAlgorithms" && exit 1
ssh -F "$ssh_config" -oKexAlgorithms=diffie-hellman-group1-sha1 default "sudo sshd -T | grep -i ^KexAlgorithms" && exit 1
ssh -F "$ssh_config" -oPubkeyAcceptedKeyTypes=ssh-rsa default "sudo sshd -T | grep -i ^PubkeyAcceptedKeyTypes" && exit 1

# -oMACs=hmac-sha1 does not break connecting, so use alternate test
(ssh -F "$ssh_config" default "sudo sshd -T | grep -i ^MACs" | grep -- -sha1) && exit 1

rm "$ssh_config"
